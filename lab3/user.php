<?php

class InvalidStringException extends Exception 
{

}

define('BCRYPT_COST', 10);
define('PASSWORD_SALT', 'password1');

Class User
{
	public $username;
	public $firstname;
	public $lastname;
	public $hash;

	public function authenticate($username, $password)
	{
		$User = $this->username;
		$guessHash = self::hash_password($password);

		//use if statements to test the functionality
		if(empty($username) || empty($password))
			throw new Exception("Username and/or Passowrd cannot be empty.");

		//use if statement to ensure good data
		if(!is_string($username) || strlen($username) < 3);

		return ($guessHash === $this->hash && $username === $User);
	}

	public static function hash_password($password)
	{
		return crypt($password, '$2a$'. BCRYPT_COST, '$', PASSWORD_SALT. '$');
	}
}
?>